package com.danalenter.karaoke.sunvel_launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AppListActivity extends Activity {
	private PackageManager pm;

	private View mLoadingContainer;
	private ListView mListView = null;
	private IAAdapter mAdapter = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_applist);

		mLoadingContainer = findViewById(R.id.loading_container);
		mListView = (ListView) findViewById(R.id.listView1);

		mAdapter = new IAAdapter(this);
		mListView.setAdapter(mAdapter);
		
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> av, View view, int position, long id) {
				//String app_name = ((TextView) view.findViewById(R.id.app_name)).getText().toString();
				String package_name = ((TextView) view.findViewById(R.id.app_package)).getText().toString();
				//Toast.makeText(AppListActivity.this, package_name, Toast.LENGTH_SHORT).show();
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(package_name);
                startActivity(launchIntent);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		startTask();
	}

	private void startTask() {
		new AppTask().execute();
	}

	private void setLoadingView(boolean isView) {
		if (isView) {
			mLoadingContainer.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.GONE);
		} else {
			mListView.setVisibility(View.VISIBLE);
			mLoadingContainer.setVisibility(View.GONE);
		}
	}

	private class ViewHolder {
		public ImageView mIcon;
		public TextView mName;
		public TextView mPacakge;
	}

	private class IAAdapter extends BaseAdapter {
		private Context mContext = null;

		private List<ApplicationInfo> mAppList = null;
		private ArrayList<AppList> mListData = new ArrayList<AppList>();

		public IAAdapter(Context mContext) {
			super();
			this.mContext = mContext;
		}

		public int getCount() {
			return mListData.size();
		}
		public Object getItem(int arg0) {
			return null;
		}
		public long getItemId(int arg0) {
			return 0;
		}
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				holder = new ViewHolder();

				LayoutInflater inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.list_item_layout, null);

				holder.mIcon = (ImageView) convertView
						.findViewById(R.id.app_icon);
				holder.mName = (TextView) convertView
						.findViewById(R.id.app_name);
				holder.mPacakge = (TextView) convertView
						.findViewById(R.id.app_package);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			AppList data = mListData.get(position);

			if (data.mIcon != null) {
				holder.mIcon.setImageDrawable(data.mIcon);
			}

			holder.mName.setText(data.mAppNaem);
			holder.mPacakge.setText(data.mAppPackge);

			return convertView;
		}

		public void rebuild() {
			if (mAppList == null) {

				Log.d("#@#", "Is Empty Application List");
				pm = AppListActivity.this.getPackageManager();
				mAppList = pm.getInstalledApplications(0);
			}

			AppList.AppFilter filter;
			//filter = AppList.THIRD_PARTY_FILTER;
			filter = null;
			if (filter != null) {
				filter.init();
			}

			mListData.clear();

			AppList addInfo = null;
			ApplicationInfo info = null;
			for (ApplicationInfo app : mAppList) {
				info = app;
				if (filter == null || filter.filterApp(info)) {
					addInfo = new AppList();
					// App Icon
					addInfo.mIcon = app.loadIcon(pm);
					// App Name
					addInfo.mAppNaem = app.loadLabel(pm).toString();
					// App Package Name
					addInfo.mAppPackge = app.packageName;
					mListData.add(addInfo);
				}
			}

			Collections.sort(mListData, AppList.ALPHA_COMPARATOR);
		}
	}

	private class AppTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			setLoadingView(true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			mAdapter.rebuild();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			mAdapter.notifyDataSetChanged();
			setLoadingView(false);
		}

	};
}