package com.danalenter.karaoke.sunvel_launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.io.IOException;

import static java.lang.Thread.sleep;

public class MainActivity extends Activity {
    final long START_DELAY = 3000;
    boolean test = false;
    Handler handle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        disableSystemUI();
        checkDisplayInfo();
        // Run loop-sound thread for midi sound
        //new Thread(startSoundRunnable).start();

        findViewById(R.id.ivLogo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goSetting();
            }
        });
        handle = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(test) {
                    goKaraoke();
                } else {
                    goKaraoke2();
                }

                test = !test;
            }
        };
    }

    void goKaraoke() {
        try {
            final String KaraokePackageName = "com.danalenter.karaoke.dalkommpartykaraoke";
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setClassName(
                    KaraokePackageName,
                    KaraokePackageName + ".activity.KaraokeActivity");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            startActivity(intent);
            overridePendingTransition(0, 0);
        } catch (Exception e) {
            Log.d("#@#", "Can not start dalkommpartykaraoke!");
            startKaraoke();
        }
    }

    void goKaraoke2() {
        try {
            //final String KaraokePackageName = "com.cecee.imove";
            final String KaraokePackageName = "com.cecee.ky_sunvel";
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setClassName(
                    KaraokePackageName,
                   // KaraokePackageName + ".activity.MainActivity");
            KaraokePackageName + ".MainActivity");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            startActivity(intent);
            overridePendingTransition(0, 0);
        } catch (Exception e) {
            Log.d("#@#", "Can not start imove!");
            startKaraoke();
        }
    }

    void startKaraoke(){
        stopKaraoke();
        handle.sendEmptyMessageDelayed(0x00, START_DELAY);
    }

    void stopKaraoke(){
        handle.removeMessages(0x00);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startKaraoke();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopKaraoke();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void goSetting(){
        startActivity(new Intent(this, SelectActivity.class));
    }

    private void disableSystemUI() {
        String exec_disableUI = "service call activity 42 s16 com.android.systemui";
        try {
            if(exec_disableUI.length() != 0)
                Runtime.getRuntime().exec(exec_disableUI);
        } catch (IOException e) {}

        Configuration validConfig = new Configuration();
        validConfig.setToDefaults();

        Configuration deltaOnlyConfig = new Configuration();
        deltaOnlyConfig.screenLayout = Configuration.SCREENLAYOUT_SIZE_XLARGE;

        validConfig.updateFrom(deltaOnlyConfig);

        return;
    }

    void checkDisplayInfo() {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager mgr = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mgr.getDefaultDisplay().getMetrics(metrics);

        StringBuffer sb = new StringBuffer();
        sb.append(String.format("\nScreen : %d x %d\n", metrics.widthPixels, metrics.heightPixels));
        sb.append(String.format("DPI : %d\n", metrics.densityDpi));

        if (metrics.densityDpi > 0) {
            String strDPI = null;
            switch (metrics.densityDpi) {
                case 120:
                    strDPI = "ldpi";
                    break;
                case 160:
                    strDPI = "mdpi";
                    break;
                case 213:
                    strDPI = "tvdpi";
                    break;
                case 240:
                    strDPI = "hdpi";
                    break;
                case 320:
                    strDPI = "xhdpi";
                    break;
                case 480:
                    strDPI = "xxhdpi";
                    break;
                case 640:
                    strDPI = "xxxhdpi";
                    break;
            }
            if (strDPI != null)
                sb.append(String.format("DPI : %s\n", strDPI));

            //sb.append(String.format("Loaded Resource : --");

            final double dpi = metrics.densityDpi;
            double dpWidth = (double) metrics.widthPixels * (160.0 / dpi);
            double dpHeight = (double) metrics.heightPixels * (160.0 / dpi);
            sb.append(String.format("Screen DP : %f x %f\n", dpWidth, dpHeight));
        }

        Log.d("DD", sb.toString());
    }

    private Runnable startSoundRunnable = new Runnable() {
        @Override
        public void run() {
            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_MUSIC, 0);
            while (true) {
                try {
                    toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
                    sleep(200);
                } catch (Exception ex) {}
            }
        }
    };
}
