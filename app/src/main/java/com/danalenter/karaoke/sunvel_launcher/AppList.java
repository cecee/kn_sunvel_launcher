package com.danalenter.karaoke.sunvel_launcher;

import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;

import java.text.Collator;
import java.util.Comparator;

public class AppList {
    public static interface AppFilter {
        public void init();
        public boolean filterApp(ApplicationInfo info);
    }

	public Drawable mIcon = null;
	public String mAppNaem = null;
	public String mAppPackge = null;

    public static final AppFilter THIRD_PARTY_FILTER = new AppFilter() {
        public void init() {
        }
        
        @Override
        public boolean filterApp(ApplicationInfo info) {
            if ((info.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                return true;
            } else if ((info.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                return true;
            }
            return false;
        }
    };

    public static final Comparator<AppList> ALPHA_COMPARATOR = new Comparator<AppList>() {
        private final Collator sCollator = Collator.getInstance();
        @Override
        public int compare(AppList object1, AppList object2) {
            return sCollator.compare(object1.mAppNaem, object2.mAppNaem);
        }
    };
}
