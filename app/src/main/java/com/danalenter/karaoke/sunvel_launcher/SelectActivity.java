package com.danalenter.karaoke.sunvel_launcher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

public class SelectActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        findViewById(R.id.btnApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goAppList();
            }
        });

//        findViewById(R.id.btnTestAni).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                goTestAni();
//            }
//        });

        findViewById(R.id.btnSetting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goSetting();
            }
        });
    }

    void goKaraoke(){
        final String KaraokePackageName = "com.danalenter.karaoke.dalkommpartykaraoke";
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setClassName(
                KaraokePackageName,
                KaraokePackageName+".activity.KaraokeActivity");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    private void goSetting(){
        startActivity(new Intent(Settings.ACTION_SETTINGS));
    }

    void goAppList(){
        Intent intent = new Intent(this, AppListActivity.class);
        startActivity(intent);
    }

    void goTestAni(){
        //Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.cecee.danal_anitest");
        //startActivity(launchIntent);
    }
}
