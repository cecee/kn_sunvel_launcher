package com.danalenter.karaoke.sunvel_launcher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;

public class ImageSlider extends SurfaceView implements SurfaceHolder.Callback{
    Display display;
    int width;
    int height;
    List<Bitmap> images;
    Bitmap targetImage;
    Bitmap baseImage;
    Paint paint;
    SurfaceHolder holder;
    Thread thread;

    public ImageSlider(Context context, int width, int height){
        super(context);
        this.width = width;
        this.height = height;
        //y = height;
        y = -height;
        x = width;
        paint = new Paint();
        paint.setColor(0xffffffff);

        holder = getHolder();
        holder.addCallback(this);
        holder.setFormat(PixelFormat.TRANSPARENT);
        setFocusable(true);
        this.setZOrderOnTop(true);

        images = new ArrayList<>();
    }

    public void addImage(Bitmap image){
        images.add(image);
        if(images.size() == 1){
            baseImage = image;
            targetImage = image;
            targetIndex = 0;
        }else if(images.size() == 2){
            targetImage = image;
            targetIndex = 1;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if(thread == null)
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true){
                        Canvas c = null;
                        try{
                            Thread.sleep(33);
                            c = holder.lockCanvas(null);
                            synchronized ((holder)){
                                doDraw(c);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            break;
                        }finally {
                            if(c != null){
                                holder.unlockCanvasAndPost(c);
                            }
                        }
                    }
                }
            });
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        stopThread();
    }

    void stopThread(){
        if(thread != null && thread.isAlive() && !thread.isInterrupted()){
            thread.interrupt();
            thread = null;
        }
    }

    int y = 0;
    int addY = 12;
    int x = 0;
    int addX = 10;
    int targetIndex = 0;
    long latestDuration = 0;
    final long WAIT_DURATION = 4000;
    boolean wait = false;
    public void doDraw(Canvas c){
        try {
            if(stop || baseImage == null || targetImage == null || baseImage.isRecycled() || targetImage.isRecycled()) return;

            int canvasWidth = c.getWidth();
            int canvasHeight = c.getHeight();
            c.drawColor(0, PorterDuff.Mode.CLEAR);
            Rect src = new Rect(0, 0, baseImage.getWidth(), baseImage.getHeight());
            Rect dst = new Rect(0, 0, canvasWidth, canvasHeight);
            c.drawBitmap(baseImage, src, dst, paint);

            if(wait){
                long current = System.currentTimeMillis();
                if(current - latestDuration >=WAIT_DURATION){
                    wait = false;
                }else{
                    return;
                }
            }

            if(images.size() > 1) {
                final int BLOCK_HEIGHT = 20;
                int MAX_ROWS = targetImage.getHeight()/BLOCK_HEIGHT;
                if(targetImage.getHeight()%BLOCK_HEIGHT >0)
                    ++MAX_ROWS;

                if(y<0)y=0;
                y+=1;
                if (y >= BLOCK_HEIGHT) {
                    src = new Rect(0, 0, targetImage.getWidth(), targetImage.getHeight());
                    c.drawBitmap(targetImage, src, dst, paint);
                    y = 0;
                    baseImage = targetImage;
                    targetIndex = (targetIndex+1) % images.size();
                    targetImage = images.get(targetIndex);
                    latestDuration = System.currentTimeMillis();
                    wait = true;
                } else {
                    for(int i=0; i<MAX_ROWS; i++) {
                        float ratio = ((float)y/(float)BLOCK_HEIGHT);
                        int srcTop = i*BLOCK_HEIGHT;
                        int srcBottom = srcTop+(int)(BLOCK_HEIGHT*ratio);
                        if(srcBottom > targetImage.getHeight())
                            srcBottom = targetImage.getHeight();
                        int dstTop = srcTop;
                        int dstBottom = srcBottom;
                        src = new Rect(0, srcTop, targetImage.getWidth(), srcBottom);
                        dst = new Rect(0, dstTop, canvasWidth, dstBottom);
                        c.drawBitmap(targetImage, src, dst, paint);
                    }
                }


            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    boolean stop = false;
    public void onDestroy(){
        stop = true;
        stopThread();

        for(Bitmap image : images){
            if(!image.isRecycled())
                image.recycle();
        }

        images.clear();
    }
}
